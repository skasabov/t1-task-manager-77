package ru.t1.skasabov.tm.config;

import org.apache.cxf.Bus;
import org.apache.cxf.jaxws.EndpointImpl;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.BeanIds;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import ru.t1.skasabov.tm.api.IAuthEndpoint;
import ru.t1.skasabov.tm.api.IProjectEndpoint;
import ru.t1.skasabov.tm.api.ITaskEndpoint;

import javax.xml.ws.Endpoint;

@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebApplicationConfiguration extends WebSecurityConfigurerAdapter {

    @Bean
    @NotNull
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @NotNull
    @Override
    @Bean(name = BeanIds.AUTHENTICATION_MANAGER)
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Override
    protected void configure(@NotNull final HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/").permitAll()
                .antMatchers("/services/*").permitAll()
                .antMatchers("/v2/api-docs").permitAll()
                .antMatchers("/configuration/ui").permitAll()
                .antMatchers("/swagger-resources/**").permitAll()
                .antMatchers("/configuration/security").permitAll()
                .antMatchers("/swagger-ui.html").permitAll()
                .antMatchers("/webjars/**").permitAll()
                .antMatchers("/api/auth/login").permitAll()

                .and()
                .exceptionHandling()
                .defaultAuthenticationEntryPointFor(new AuthenticationEntryPoint(), new AntPathRequestMatcher("/login"))

                .and()
                .authorizeRequests()
                .anyRequest().authenticated()
                .and()
                .formLogin()
                .and()
                .logout().permitAll()
                .logoutSuccessUrl("/login")
                .and()
                .csrf().disable();
    }

    @Bean
    @NotNull
    public Endpoint projectEndpointRegistry(
            @NotNull final IProjectEndpoint projectEndpoint,
            @NotNull final Bus bus
    ) {
        @NotNull final EndpointImpl endpoint = new EndpointImpl(bus, projectEndpoint);
        endpoint.publish("/ProjectEndpoint");
        return endpoint;
    }

    @Bean
    @NotNull
    public Endpoint taskEndpointRegistry(
            @NotNull final ITaskEndpoint taskEndpoint,
            @NotNull final Bus bus
    ) {
        @NotNull final EndpointImpl endpoint = new EndpointImpl(bus, taskEndpoint);
        endpoint.publish("/TaskEndpoint");
        return endpoint;
    }

    @Bean
    @NotNull
    public Endpoint authEndpointRegistry(
            @NotNull final IAuthEndpoint authEndpoint,
            @NotNull final Bus bus
    ) {
        @NotNull final EndpointImpl endpoint = new EndpointImpl(bus, authEndpoint);
        endpoint.publish("/AuthEndpoint");
        return endpoint;
    }

}
