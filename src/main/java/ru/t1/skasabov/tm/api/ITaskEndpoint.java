package ru.t1.skasabov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.skasabov.tm.dto.TaskDto;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface ITaskEndpoint {

    @Nullable
    @WebMethod
    List<TaskDto> findAll();

    @NotNull
    @WebMethod
    TaskDto save(@NotNull @WebParam(name = "task", partName = "task") TaskDto task);

    @Nullable
    @WebMethod
    TaskDto findById(@NotNull @WebParam(name = "id", partName = "id") String id);

    @WebMethod
    boolean existsById(@NotNull @WebParam(name = "id", partName = "id") String id);

    @WebMethod
    long count();

    @WebMethod
    void deleteById(@NotNull @WebParam(name = "id", partName = "id") String id);

    @WebMethod
    void delete(@NotNull @WebParam(name = "task", partName = "task") TaskDto task);

    @WebMethod
    void deleteAll(@Nullable @WebParam(name = "tasks", partName = "tasks") List<TaskDto> tasks);

    @WebMethod
    void clear();

}
