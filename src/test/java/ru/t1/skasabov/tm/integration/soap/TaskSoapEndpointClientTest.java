package ru.t1.skasabov.tm.integration.soap;

import lombok.SneakyThrows;
import org.apache.cxf.helpers.CastUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.http.HttpHeaders;
import ru.t1.skasabov.tm.api.IAuthEndpoint;
import ru.t1.skasabov.tm.api.ITaskEndpoint;
import ru.t1.skasabov.tm.client.soap.AuthSoapEndpointClient;
import ru.t1.skasabov.tm.client.soap.TaskSoapEndpointClient;
import ru.t1.skasabov.tm.dto.TaskDto;
import ru.t1.skasabov.tm.dto.UserDto;
import ru.t1.skasabov.tm.endpoint.AuthEndpointImpl;
import ru.t1.skasabov.tm.endpoint.TaskEndpointImpl;
import ru.t1.skasabov.tm.marker.IntegrationCategory;

import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.soap.SOAPFaultException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TaskSoapEndpointClientTest {

    private static final int NUMBER_OF_ENTITIES = 4;

    @NotNull
    private static final String BASE_URL = "http://localhost:8080";

    @NotNull
    private static ITaskEndpoint TASK_ENDPOINT = new TaskEndpointImpl();

    @NotNull
    private static IAuthEndpoint AUTH_ENDPOINT = new AuthEndpointImpl();

    @NotNull
    private final TaskDto task1 = new TaskDto("Test Task 1");

    @NotNull
    private final TaskDto task2 = new TaskDto("Test Task 2");

    @NotNull
    private final TaskDto task3 = new TaskDto("Test Task 3");

    @NotNull
    private final TaskDto task4 = new TaskDto("Test Task 4");

    private boolean isAuth = false;

    @NotNull
    private String userId;

    @NotNull
    private List<TaskDto> taskList = new ArrayList<>();

    @BeforeClass
    @SneakyThrows
    @SuppressWarnings({"unchecked", "rawtypes"})
    public static void initEndpoint() {
        AUTH_ENDPOINT = AuthSoapEndpointClient.getInstance(BASE_URL);
        TASK_ENDPOINT = TaskSoapEndpointClient.getInstance(BASE_URL);
        Assert.assertTrue(AUTH_ENDPOINT.login("test", "test").isSuccess());
        @NotNull final BindingProvider authBindingProvider = (BindingProvider) AUTH_ENDPOINT;
        @NotNull final BindingProvider taskBindingProvider = (BindingProvider) TASK_ENDPOINT;
        @Nullable Map<String, List<String>> headers = CastUtils.cast((Map) authBindingProvider.getResponseContext().get(MessageContext.HTTP_RESPONSE_HEADERS));
        if (headers == null) headers = new HashMap<>();
        @NotNull final Object cookieValue = headers.get(HttpHeaders.SET_COOKIE);
        @NotNull final List<String> cookies = (List<String>) cookieValue;
        headers.put("Cookie", Collections.singletonList(cookies.get(0)));
        taskBindingProvider.getRequestContext().put(MessageContext.HTTP_REQUEST_HEADERS, headers);
    }

    @AfterClass
    public static void logout() {
        AUTH_ENDPOINT.logout();
    }

    @Before
    public void initTest() {
        @Nullable final UserDto user = AUTH_ENDPOINT.profile();
        Assert.assertNotNull(user);
        userId = user.getId();
        task1.setUserId(userId);
        task2.setUserId(userId);
        task3.setUserId(userId);
        task4.setUserId(userId);
        @Nullable final List<TaskDto> tasks = TASK_ENDPOINT.findAll();
        taskList = tasks == null ? Collections.emptyList() : tasks;
        TASK_ENDPOINT.clear();
        TASK_ENDPOINT.save(task1);
        TASK_ENDPOINT.save(task2);
        TASK_ENDPOINT.save(task3);
        TASK_ENDPOINT.save(task4);
    }

    @After
    public void clean() {
        if (isAuth) {
            Assert.assertTrue(AUTH_ENDPOINT.login("test", "test").isSuccess());
        }
        TASK_ENDPOINT.clear();
        for (@NotNull final TaskDto task : taskList) TASK_ENDPOINT.save(task);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void findAllTest() {
        @Nullable final List<TaskDto> tasks = TASK_ENDPOINT.findAll();
        Assert.assertNotNull(tasks);
        Assert.assertEquals(NUMBER_OF_ENTITIES, tasks.size());
    }

    @Category(IntegrationCategory.class)
    @Test(expected = SOAPFaultException.class)
    public void findAllNoAuthTest() {
        isAuth = true;
        AUTH_ENDPOINT.logout();
        TASK_ENDPOINT.findAll();
    }

    @Test
    @Category(IntegrationCategory.class)
    public void saveTest() {
        @NotNull final TaskDto task = new TaskDto("Test Task");
        task.setUserId(userId);
        TASK_ENDPOINT.save(task);
        Assert.assertEquals(NUMBER_OF_ENTITIES + 1, TASK_ENDPOINT.count());
    }

    @Category(IntegrationCategory.class)
    @Test(expected = SOAPFaultException.class)
    public void saveNoAuthTest() {
        isAuth = true;
        AUTH_ENDPOINT.logout();
        @NotNull final TaskDto task = new TaskDto("Test Task");
        task.setUserId(userId);
        TASK_ENDPOINT.save(task);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void findByIdTest() {
        @Nullable final TaskDto Task = TASK_ENDPOINT.findById(task1.getId());
        Assert.assertNotNull(Task);
        Assert.assertEquals("Test Task 1", Task.getName());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void findByEmptyIdTest() {
        Assert.assertNull(TASK_ENDPOINT.findById(""));
    }

    @Test
    @Category(IntegrationCategory.class)
    public void findByInvalidIdTest() {
        Assert.assertNull(TASK_ENDPOINT.findById("123"));
    }

    @Category(IntegrationCategory.class)
    @Test(expected = SOAPFaultException.class)
    public void findByIdNoAuthTest() {
        isAuth = true;
        AUTH_ENDPOINT.logout();
        TASK_ENDPOINT.findById(task1.getId());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void existsByIdTest() {
        Assert.assertTrue(TASK_ENDPOINT.existsById(task1.getId()));
    }

    @Test
    @Category(IntegrationCategory.class)
    public void existsByEmptyIdTest() {
        Assert.assertFalse(TASK_ENDPOINT.existsById(""));
    }

    @Test
    @Category(IntegrationCategory.class)
    public void existsByInvalidIdTest() {
        Assert.assertFalse(TASK_ENDPOINT.existsById("123"));
    }

    @Category(IntegrationCategory.class)
    @Test(expected = SOAPFaultException.class)
    public void existsByIdNoAuthTest() {
        isAuth = true;
        AUTH_ENDPOINT.logout();
        TASK_ENDPOINT.existsById(task1.getId());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void countTest() {
        Assert.assertEquals(NUMBER_OF_ENTITIES, TASK_ENDPOINT.count());
    }

    @Category(IntegrationCategory.class)
    @Test(expected = SOAPFaultException.class)
    public void countNoAuthTest() {
        isAuth = true;
        AUTH_ENDPOINT.logout();
        TASK_ENDPOINT.count();
    }

    @Test
    @Category(IntegrationCategory.class)
    public void deleteByIdTest() {
        TASK_ENDPOINT.deleteById(task1.getId());
        Assert.assertEquals(NUMBER_OF_ENTITIES - 1, TASK_ENDPOINT.count());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void deleteByEmptyIdTest() {
        TASK_ENDPOINT.deleteById("");
        Assert.assertEquals(NUMBER_OF_ENTITIES, TASK_ENDPOINT.count());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void deleteByInvalidIdTest() {
        TASK_ENDPOINT.deleteById("some_id");
        Assert.assertEquals(NUMBER_OF_ENTITIES, TASK_ENDPOINT.count());
    }

    @Category(IntegrationCategory.class)
    @Test(expected = SOAPFaultException.class)
    public void deleteByIdNoAuthTest() {
        isAuth = true;
        AUTH_ENDPOINT.logout();
        TASK_ENDPOINT.deleteById(task1.getId());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void deleteTest() {
        TASK_ENDPOINT.delete(task1);
        Assert.assertEquals(NUMBER_OF_ENTITIES - 1, TASK_ENDPOINT.count());
    }

    @Category(IntegrationCategory.class)
    @Test(expected = SOAPFaultException.class)
    public void deleteNoAuthTest() {
        isAuth = true;
        AUTH_ENDPOINT.logout();
        TASK_ENDPOINT.delete(task1);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void deleteTasksTest() {
        @NotNull final List<TaskDto> tasks = new ArrayList<>();
        tasks.add(task2);
        tasks.add(task3);
        tasks.add(task4);
        TASK_ENDPOINT.deleteAll(tasks);
        Assert.assertEquals(NUMBER_OF_ENTITIES - tasks.size(), TASK_ENDPOINT.count());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void deleteEmptyTasksTest() {
        TASK_ENDPOINT.deleteAll(Collections.emptyList());
        Assert.assertEquals(NUMBER_OF_ENTITIES, TASK_ENDPOINT.count());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void deleteNullTasksTest() {
        TASK_ENDPOINT.deleteAll(null);
        Assert.assertEquals(NUMBER_OF_ENTITIES, TASK_ENDPOINT.count());
    }

    @Category(IntegrationCategory.class)
    @Test(expected = SOAPFaultException.class)
    public void deleteProjectsNoAuthTest() {
        isAuth = true;
        AUTH_ENDPOINT.logout();
        @NotNull final List<TaskDto> tasks = new ArrayList<>();
        tasks.add(task2);
        tasks.add(task3);
        tasks.add(task4);
        TASK_ENDPOINT.deleteAll(tasks);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void clearTest() {
        TASK_ENDPOINT.clear();
        Assert.assertEquals(0, TASK_ENDPOINT.count());
    }

    @Category(IntegrationCategory.class)
    @Test(expected = SOAPFaultException.class)
    public void clearNoAuthTest() {
        isAuth = true;
        AUTH_ENDPOINT.logout();
        TASK_ENDPOINT.clear();
    }

}
